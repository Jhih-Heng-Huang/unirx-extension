using System;
using System.Linq;
using UnityEngine;
using UniRx;

namespace UniRxExtension
{
	public static class KeyStream
	{
		public static IObservable<KeyCode> BindKeyFromMilliseconds(this IObservable<KeyCode> keyStream, double milliseconds, params KeyCode[] keyCodes)
		{
			Func<TimeSpan, KeyCode[], IObservable<KeyCode>> takeKeyInTimeStream =
				(TimeSpan time, KeyCode[] keys) => 
				keyStream.Take(time).Take(1).Where(key => keys.Any(k => k == key));
			
			return keyStream.SelectMany(_ => takeKeyInTimeStream(TimeSpan.FromMilliseconds(milliseconds), keyCodes));
		}

		public static IObservable<KeyCode> GenKeyDownStream(params KeyCode[] keyCodes)
		=> GenKeyActionStream(Input.GetKeyDown, keyCodes);

		public static IObservable<KeyCode> GenKeyOnStream(params KeyCode[] keyCodes)
		=> GenKeyActionStream(Input.GetKey, keyCodes);

		public static IObservable<KeyCode> GenKeyUpStream(params KeyCode[] keyCodes)
		=> GenKeyActionStream(Input.GetKeyUp, keyCodes);

		public static IObservable<KeyCode> GenKeyActionStream(Func<KeyCode, bool> keyAction, params KeyCode[] keyCodes)
		{
			var keys = Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>().ToList();

			return Observable.EveryUpdate()
				.Select(_ => keys.FirstOrDefault(key => keyAction(key)))
				.Where(key => key != KeyCode.None).Where(key => keyCodes.Any(keyCode => keyCode == key));
		}
	}	
}